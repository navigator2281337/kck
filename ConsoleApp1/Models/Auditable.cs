﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    public class Auditable
    {
        private List<TextSnapshot> _textHistory = new List<TextSnapshot>();

  

        public int NumberOfUpdates
        {
            get
            {
                return _textHistory.Count();
            }
        }


        public DateTime LastUpdate
        {
            get 
            {
                return _textHistory.Last().ChangeTime;
            }
        }

        private string _text;

        public string Text
        {
            get { return _text; }
            set
            {
                _textHistory.Add(new TextSnapshot() { Cause = _text, ChangeTime = DateTime.Now });

                _text = value;
            }
        }

        private struct TextSnapshot
        {
            public string Cause;
            public DateTime ChangeTime;
        }

        public Auditable(string text)
        { 
            Text = text;
        }

        public string[] UpdateHistory()
        {
            string[] array = new string[NumberOfUpdates];
            int i = 0;
            
            foreach (TextSnapshot item in _textHistory)
            {
                array[i] = item.Cause;
                i++;
            }

            return array;
        }
    }
}
