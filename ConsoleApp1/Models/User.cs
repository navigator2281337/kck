﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public Auditable PasswordHash { get; set; }

        public User()
        {

        }

        public User(int id, string login, string password)
        {
            Id = id;
            Login = login;
            PasswordHash.Text = GetHash(password);
        }

        public override string ToString()
        {
            return "Id: " + Id + " Login: " + Login + " Hash " + PasswordHash.Text;
        }

        public bool IsAuth(string password)
        {
            string passwordH = GetHash(password);

            if (passwordH == PasswordHash.Text)
            {
                return true;
            }
            return false;
        }

        public bool IsPasswordUsed(string password)
        {
            string passwordH = GetHash(password);

            var array = PasswordHash.UpdateHistory();

            foreach (var item in array)
            {
                string hash = GetHash(item);
                if (hash == passwordH)
                {
                    return true;
                }
            }
            return false;
        }

        public bool ChangePassword(string password)
        {
            foreach (var item in PasswordHash.UpdateHistory())
            {
                if (password == item)
                {
                    return true;
                }
                else
                {
                    string passwordHash = GetHash(password);
                    PasswordHash = new Auditable(passwordHash);
                }
            }
            return false;
        }

        
        private string GetHash(string password)
        {
            using (SHA256CryptoServiceProvider sha256Hash = new SHA256CryptoServiceProvider())
            {
                Byte[] inputBytes = Encoding.UTF8.GetBytes(password);

                Byte[] hashedBytes = sha256Hash.ComputeHash(inputBytes);

                var passwordH = BitConverter.ToString(hashedBytes);

                return passwordH;
            }
          
        }
    }
}
