﻿using ConsoleApp1.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace kck.Models
{
    public class RepairRequest : IComparable
    {
        public int RequestNumber { get;}
        public DateTime Date { get;}
        public Auditable Cause { get; }
        public int ApartmentNumber { get; set; }
        public List<Comment> Comments { get;}


        public RepairRequest()
        {
            ConrentGenerator conrentGenerator = new ConrentGenerator();
            this.RequestNumber = conrentGenerator.GenerateId();
            this.Date = conrentGenerator.GenerateDateTime();
            this.Cause = new Auditable(conrentGenerator.GenerateOccasionCause());
            this.ApartmentNumber = conrentGenerator.GenerateApparmentNumber();       
        }

        public override string ToString()
        {
            return "Заявка №" + RequestNumber + " от " + Date + ". " + Cause + ". Номер квартиры:" + ApartmentNumber + ".";
        }


        public int CompareTo(object obj)
        {
            RepairRequest rr = obj as RepairRequest;
            if (rr != null)
            {
                int result = Date.CompareTo(rr.Date);
                if (result != 0)
                { 
                    return result;
                }
                      result = RequestNumber.CompareTo(rr.RequestNumber);
                if (result != 0)
                { 
                    return result;
                }
             return result = Cause.Text.CompareTo(rr.Cause.Text);
            }
            else
            {
                throw new Exception("Параметр должен быть типа RepairRequest");
            }
        }
    }
}
