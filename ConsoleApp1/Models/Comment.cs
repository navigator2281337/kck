﻿using kck.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    public class Comment : IComparable
    {
        public int Id { get; set; }
        public DateTime Create { get; set; }
        public List<Comment> Comments { get; }
        public DateTime LastUpdate { get; set; } = DateTime.Now;
        public int NumberOfUpdates { get; set; }
        Auditable Text { get; }



        public Comment()
        {
            ConrentGenerator conrentGenerator = new ConrentGenerator();
            this.Id = conrentGenerator.GenerateId();
            this.Create = conrentGenerator.GenerateDateTime();
            this.Text = new Auditable(conrentGenerator.GenerateCommentText());
        }

        public override string ToString()
        {
            return "№" + Id + " " + Text + ". Дата: " + Create + ".";
        }

        public int CompareTo(object obj)
        {
            Comment comment = obj as Comment;
            if (comment != null)
            {
                int result = Text.Text.CompareTo(comment.Text.Text);
                 return result;
            }
            else
            {
                throw new Exception("Параметр должен быть типа Comment");
            }
        }
    }
}
