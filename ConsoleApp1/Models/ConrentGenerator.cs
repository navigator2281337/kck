﻿using kck.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    public class ConrentGenerator
    {
        private Random _rand;

        public ConrentGenerator()
        {
            _rand = new Random();
        }

        public int GenerateId()
        {
            const int startGenNumber = 111111;
            const int endGenNumber = 999999;

            var requestNumber = _rand.Next(startGenNumber, endGenNumber + 1);

            return requestNumber;
        }

        public int GenerateId(int min, int max)
        {

            var requestNumber = _rand.Next(min, max + 1);

            return requestNumber;
        }

        public string GenerateOccasionCause()
        {
            string[] сauses = new[]
            {
                "Протечка трубы", "Отключтли свет", "Сломалась лестница"
            };
                         
            var сause = сauses[_rand.Next(сauses.Length)];
           
            return сause;
        }

      

        public int GenerateApparmentNumber()
        {
            const int numberOfApartmentsInTheHouse = 90;
            const int firstApartmentsInTheHouse = 1;

            var number = _rand.Next(firstApartmentsInTheHouse, numberOfApartmentsInTheHouse + 1);

            return number;
        }

        public DateTime GenerateDateTime()
        {
            const int secondInDay = 86400;
            const int startSecondInDay = 1;

            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            var dateTime =  start.AddDays(_rand.Next(range)).AddSeconds(_rand.Next(startSecondInDay, secondInDay + 1));

            return dateTime;
        }

        public string GenerateCommentText()
        {
            string[] comments = new[]
            {
                "Проблема устранена", "Принят, в очереди на ремонт", "Идет ремонт"
            };

            var comment = comments[_rand.Next(comments.Length)];

            return comment;
        }
        
        private string GenALphavite()
        {
            int n = 5;
            char[,] a = new char[5, 5];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    a[i, j] = (char)_rand.Next(0x0410, 0x44F);
                    return a[i, j] + "@mail.ru";
                }
                return " ";
            }
            return "";
        }

        public static User GenerateUser()
        {
            User user = new User();
            ConrentGenerator Cg = new ConrentGenerator(); 


                user.Id = Cg.GenerateId(1, 1000);
                user.Login = Cg.GenALphavite();
                user.PasswordHash = new Auditable("5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5");

            return user;
        }

        public static RepairRequest GenerateRepairRequest()
        {
            return new RepairRequest();
        }

        public static Comment GenerateComment()
        {
            return new Comment();
        }


    }
}
