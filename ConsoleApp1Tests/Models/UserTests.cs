﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models.Tests
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void TestIsAuth()
        {
            string a = "qwerty";

            User user = new User();

            user.Id = 1;
            user.Login = "login";
            user.PasswordHash = new Auditable("5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5");



            bool actual = user.IsAuth(a);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsPasswordUsed()
        {
            string a = "qwerty";

            User user = new User();

            user.Id = 1;
            user.Login = "login";
            user.PasswordHash = new Auditable("5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5");
            bool actual = user.IsPasswordUsed(a);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestChangePassword()
        {
            string a = "qwerty";

            User user = new User();

            user.Id = 1;
            user.Login = "login";
            user.PasswordHash = new Auditable("5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5");
            bool actual = user.ChangePassword(a);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestChangePasswordNew()
        {
            string data = "11";

            User user = new User();

            user.Id = 1;
            user.Login = "login";
            user.PasswordHash = new Auditable("11");
            user.ChangePassword("22");
            user.ChangePassword("33");
            user.ChangePassword("44");

            bool actual = user.ChangePassword(data);

            Assert.IsTrue(actual);
        }

    }
}