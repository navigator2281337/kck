﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;



namespace ConsoleApp1.Tests
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void TestIsAuth()
        {
            string a = "qwerty";
            bool expected = false;

            User user = new User();
            bool actual = user.IsAuth(a);

            Assert.AreEqual(expected, actual);
        }

        public void TestIsPasswordUsed()
        {
            string a = "qwerty";
            bool expected = false;

            User user = new User();
            bool actual = user.IsPasswordUsed(a);

            Assert.AreEqual(expected, actual);
        } 
        
        public void TestChangePassword()
        {
            string a = "qwerty";
            bool expected = false;

            User user = new User();
            bool actual = user.ChangePassword(a);

            Assert.AreEqual(expected, actual);
        }
    }
}
